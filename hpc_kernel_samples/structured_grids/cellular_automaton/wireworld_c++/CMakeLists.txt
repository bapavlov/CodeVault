# Packages are optional: if they are not present, certain code samples are not compiled
cmake_minimum_required(VERSION 2.8.10 FATAL_ERROR)

find_package(MPI)      # Built-in in CMake
find_package(Boost 1.55 COMPONENTS program_options REQUIRED)

include(${CMAKE_CURRENT_SOURCE_DIR}/../../../cmake/common.cmake)

# ==================================================================================================

if ("${DWARF_PREFIX}" STREQUAL "")
  set(DWARF_PREFIX 5_structured)
endif()

set(NAME ${DWARF_PREFIX}_wireworld)

if (MPI_FOUND AND Boost_FOUND)
	enable_language(CXX)
    include_directories(${MPI_INCLUDE_PATH} ${Boost_INCLUDE_DIRS})
    add_executable(${NAME} main.cpp Configuration.cpp Communicator.cpp P2PCommunicator.cpp CollectiveCommunicator.cpp FileIO.cpp MpiEnvironment.cpp MpiSubarray.cpp MpiWireworld.cpp Tile.cpp Util.cpp)
    set(CMAKE_BUILD_TYPE RelWithDebInfo)
    if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
    	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -march=native -Wall -Wextra")
	elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
    	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -march=native -Weverything -Wno-missing-prototypes -Wno-covered-switch-default -Wno-switch-enum -Wno-padded -Wno-c++98-compat -Wno-c++98-compat-pedantic")
    elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel")
    	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -xHost -std=c++14 -Wall")
    endif()
    set_target_properties(${NAME} PROPERTIES CXX_STANDARD 14 CXX_STANDARD_REQUIRED YES)
    target_link_libraries(${NAME} ${MPI_LIBRARIES} ${Boost_LIBRARIES})
    install(TARGETS ${NAME} DESTINATION bin)
    message("** Enabling '${NAME}': with MPI and Boost")
else()
    message("## Skipping '${NAME}': MPI or Boost support missing")
#    dummy_install(${NAME} "MPI")
endif()

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${C_FLAGS}")

unset(NAME)
# ==================================================================================================
