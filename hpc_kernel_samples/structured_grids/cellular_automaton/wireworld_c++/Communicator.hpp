#pragma once

#include <boost/container/static_vector.hpp>
#include <mpi.h>

#include "Configuration.hpp"
#include "MpiEnvironment.hpp"
#include "MpiRequest.hpp"
#include "State.hpp"
#include "Util.hpp"

// creates the graph topology and does the communication
class Communicator {
	constexpr static std::size_t NoNeighbors{8};

	// for very small container with known max size, this is a performance
	// improvement
	template <typename T>
	using Vector = boost::container::static_vector<T, NoNeighbors>;

  protected:
	// data members for graph topology
	Vector<int> neighbors_;
	Vector<int> sizes_;
	Vector<MPI_Datatype> sendTypes_;
	const Vector<MPI_Datatype>& recvTypes_{sendTypes_};
	Vector<MPI_Aint> sendDisplacements_;
	Vector<MPI_Aint> recvDisplacements_;
	MPI_Comm commDistGraph_{MPI_COMM_NULL};

	// data types
	MPI_Datatype haloRowType_{};
	MPI_Datatype haloColumnType_{};
	MPI_Datatype haloCornerType_{MPI_CHAR};

  public:
	Communicator() = default;
	Communicator(const MpiEnvironment& env,
	             const Size& gridSize, const Size& tileSize);
	virtual ~Communicator();
	void swap(Communicator& first, Communicator& second);
	Communicator(Communicator&) = delete;
	Communicator& operator=(Communicator&) = delete;
	Communicator(Communicator&& other) noexcept;
	Communicator& operator=(Communicator&& other) noexcept;

	virtual void Communicate(State* model) = 0;
	virtual MpiRequest AsyncCommunicate(State* model) = 0;
};
