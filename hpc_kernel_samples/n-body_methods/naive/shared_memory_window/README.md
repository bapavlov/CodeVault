# README - MPI with shared memory window (Naive n-Body Example)

## Description

This sample is a simple particle simulation in C++ 14 using only MPI. The code is explicitly not optmized for better understanding. The idea is to run n MPI processes per compute node. Within a node, all MPI processes share the same memory, so no communication is needed within a node.

This code sample demonstates:
 * **creating comunicators**, i.e. `MPI_Comm_split`
 * **allocating shared memory**, i.e. `MPI_Win_allocate_shared`
 * **collective communication**, i.e. `MPI_Bcast`, `MPI_Allgather`
 * How to use MPI-Datatypes to **send and receive non-contiguous structured data** directly, avoiding send and receive buffer packing and unpacking.
 
 The code sample is structures as followed:
 
  * `Configuration.hpp`: Configuration structure, command-line parsing.
  * `MpiComm`: Wrapper class for an MPI_Comm object
  * `MpiEnvironment`: Wrapper class for the MPI Environment
  * `MpiSharedMemWin`: Wrapper class for MPI Shared Memory Window
  * `main.cpp`: The main program.
  * `MpiTypes.hpp`: Code for initialization of custom MPI-Datatypes.
  * `Particle.hpp`: Particle structure.
  * `Vec3.hpp`: 3D-Vector structure, arithmetrical operators
  * `gsl/*`: Guideline Support Library [C++ Core Guidelines](http://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines), is a utility library. (https://github.com/Microsoft/GSL)
  
## Release Date

2016-07-27


## Version History

 * 2016-07-27 Initial Release on PRACE CodeVault repository


## Contributors

 * Thomas Steinreiter - [thomas.steinreiter@risc-software.at](mailto:thomas.steinreiter@risc-software.at)


## Copyright

This code is available under Apache License, Version 2.0 - see also the license file in the CodeVault root directory. The GSL library is available under MIT.


## Languages

This sample is written in C++ 14.


## Parallelisation

This sample uses MPI-3 for parallelisation.


## Level of the code sample complexity

Intermediate / Advanced


## Compiling

Follow the compilation instructions given in the main directory of the kernel samples directory (`/hpc_kernel_samples/README.md`).

## Running

To run the program, use something similar to

    mpiexec -n [nprocs] ./4_nbody_naive__shared_mem -i [ni] -p [np]

either on the command line or in your batch script, where `ni` specifies the number of iterations and `np` specify the number of particles. If missing, the value `1000` is used.


### Command line arguments

 * `-i [iterations]`: specify the number of iterations, which are simulated
 * `-p [particles]`: specify the number of particles in the simulation
 
### Example
 
If you run

	 mpiexec -n 4 ./4_nbody_naive__shared_mem  -p 1000 -i 1000

the output should look similar to

	Creating MPI communicators...

	rank[0]: Size of intranode_comm: 2
	
	rank[2]: Size of intranode_comm: 2
	
	rank[0]: Size of internode_comm: 2
	
	END Creating MPI communicators...
	
	Creating shared memory region (56000 bytes)...
	Initialization done:
	      0X:  -0.7592153
	      1X: -0.03729511
	      2X:  0.02902422
	      3X:   0.3433951
	      4X:  -0.6270536
	      5X:  -0.4882588
	      6X:  -0.6301843
	      7X:  -0.3185952
	      8X:   -0.452981
	      9X:  -0.6942754
	     10X:  -0.2393945
	End Simulation
	      0X:   -0.772929
	      1X: -0.03069742
	      2X:  0.03063559
	      3X:   0.3442205
	      4X:  -0.6436881
	      5X:  -0.4946151
	      6X:  -0.6532033
	      7X:   -0.328519
	      8X:  -0.4519648
	      9X:  -0.7137452
	     10X:  -0.2450726
	