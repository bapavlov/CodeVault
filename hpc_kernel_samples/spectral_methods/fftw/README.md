=======
README
=======

# 1. Code sample name
fftw

# 2. Description of the code sample package
This example demonstrates the use of a fast Fourier transform library for CPUs: FFTW3. The example is set-up to perform an in-place forward transform followed by an in-place inverse transform such that the output signal should be equal to the input signal. The example uses pre-processor defines to select the FFT-size and the number of batches. The FFTs performed are 1D and double-precision complex-to-complex.

Additional pre-requisites:
* FFTW3 library

See http://www.fftw.org/fftw3_doc for the full FFTW3 documentation.

# 3. Release date
20 July 2015

# 4. Version history 
1.0

# 5. Contributor (s) / Maintainer(s) 
Cedric Nugteren <cedric.nugteren@surfsara.nl>

# 6. Copyright / License of the code sample
Apache 2.0

# 7. Language(s) 
C++

# 8. Parallelisation Implementation(s)
Sequential

# 9. Level of the code sample complexity 
Basic level, uses library calls only

# 10. Instructions on how to compile the code
Uses the CodeVault CMake infrastructure, see main README.md

# 11. Instructions on how to run the code
No command-line arguments, simply run the executable

# 12. Sample input(s)
Input-data is generated automatically when running the program.

# 13. Sample output(s)
Output data is verified programmatically after doing a forward and reverse FFT
